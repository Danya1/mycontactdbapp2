package com.example.mycontactdbapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button buttonAddContact, buttonClearAllContacts;
    EditText editTextName, editTextPhone, editTextNote;

    ListView listViewContacts;

    TableContacts tableContacts;

    private void UpdateListViewContacts()
    {
        ContactsAdapter contactsAdapter = new ContactsAdapter(getApplicationContext(), tableContacts.GetAll());
        listViewContacts.setAdapter(contactsAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = findViewById(R.id.editTextName);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextNote = findViewById(R.id.editTextNote);

        buttonAddContact = findViewById(R.id.buttonAddContact);
        buttonAddContact.setOnClickListener(buttonAddContactClickListener);

        buttonClearAllContacts = findViewById(R.id.buttonClearAllContacts);
        buttonClearAllContacts.setOnClickListener(buttonClearAllContactsClickListener);

        listViewContacts = findViewById(R.id.listViewContacts);

        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("app.db",MODE_PRIVATE,null);
        tableContacts = new TableContacts(db);

        UpdateListViewContacts();
    }

    View.OnClickListener buttonAddContactClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String name = editTextName.getText().toString();
            String phone = editTextPhone.getText().toString();
            String note = editTextNote.getText().toString();

            if(name.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Name!",Toast.LENGTH_LONG).show();
                return;
            }

            if(phone.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Input Phone!",Toast.LENGTH_LONG).show();
                return;
            }

            tableContacts.AddNew(new Contact(0, name, phone, note));
            UpdateListViewContacts();
            //todo add new and update list view

            editTextName.getText().clear();
            editTextPhone.getText().clear();
            editTextNote.getText().clear();
        }
    };

    View.OnClickListener buttonClearAllContactsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            tableContacts.DeleteAll();

            Toast.makeText(getApplicationContext(),"Contacts deleted",Toast.LENGTH_LONG).show();

            UpdateListViewContacts();
            //todo update list view
        }
    };
}
